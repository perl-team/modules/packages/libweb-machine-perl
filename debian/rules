#!/usr/bin/make -f
# -*- mode: makefile; coding: utf-8 -*-
# Copyright 2016-2017, Jonas Smedegaard <dr@jones.dk>
# Description: Main Debian packaging script for Web::Machine
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

include /usr/share/cdbs/1/rules/upstream-tarball.mk
include /usr/share/cdbs/1/rules/utils.mk
include /usr/share/cdbs/1/class/perl-makemaker.mk
include /usr/share/cdbs/1/rules/debhelper.mk

pkg = $(DEB_SOURCE_PACKAGE)

DEB_UPSTREAM_PACKAGE = Web-Machine
DEB_UPSTREAM_URL = http://www.cpan.org/modules/by-module/Web

# Needed by upstream build and (always) at runtime
perl-deps = http-headers-actionpack http-message hash-multivalue
perl-deps += io-handle-util module-runtime plack sub-exporter try-tiny
deps = $(patsubst %,$(comma) lib%-perl,$(perl-deps))

# Needed (always/optionally) by upstream testsuite
perl-bdeps-test = net-http test-failwarnings test-fatal
bdeps-test = $(patsubst %,$(comma) lib%-perl,$(perl-bdeps-test))
bdeps-test-opt = libjson-perl, libjson-xs-perl

CDBS_BUILD_DEPENDS +=, $(deps), $(bdeps-test), $(bdeps-test-opt)
CDBS_DEPENDS_$(pkg) = $(deps)

DEB_INSTALL_EXAMPLES_$(pkg) += examples/*
