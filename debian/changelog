libweb-machine-perl (0.17-4) unstable; urgency=medium

  * Team upload.
  * Remove Makefile.old via debian/clean. (Closes: #1048133)

 -- gregor herrmann <gregoa@debian.org>  Fri, 08 Mar 2024 17:34:29 +0100

libweb-machine-perl (0.17-3) unstable; urgency=medium

  * Team upload.
  * Fix autopkgtests: the smoke test also needs the examples/ directory.

 -- gregor herrmann <gregoa@debian.org>  Sun, 03 Jul 2022 21:28:19 +0200

libweb-machine-perl (0.17-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/*: update GitHub URLs to use HTTPS.

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Bump debhelper dependency to >= 9, since that's what is used in
    debian/compat.
  * Bump debhelper from old 9 to 10.
  * Set Testsuite header for perl package.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 28 Jun 2022 22:16:53 +0100

libweb-machine-perl (0.17-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Fri, 08 Jan 2021 02:41:21 +0100

libweb-machine-perl (0.17-1) unstable; urgency=medium

  [ upstream ]
  * New release.
    + Fix bind_path() from Web::Machine::Util to handle path parts which
      evaluates to false, like "/user/0".

  [ Jonas Smedegaard ]
  * Update git-buildpage config: Filter any .git* file.
  * Modernize Vcs-* fields:
    + Use git subdir (not cgit).
    + Add .git suffix for Vcs-Git URL.
  * Declare compliance with Debian Policy 3.9.8.
  * Stop override lintian for
    package-needs-versioned-debhelper-build-depends: Fixed in lintian.
  * Update watch file:
    + Track only MetaCPAN URL.
    + Tighten version regex.
  * Update copyright info:
    + Bump (yes, not extend) coverage for upstream author.
    + Extend coverage of Debian packaging.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 26 Jan 2017 14:43:27 +0100

libweb-machine-perl (0.16-2) unstable; urgency=medium

  * Include examples.
  * Update package relations:
    + Build-depend on libjson-perl libjson-xs-perl: Needed for optional
      parts of testsuite.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 10 Mar 2016 16:38:20 +0100

libweb-machine-perl (0.16-1) unstable; urgency=low

  * Initial packaging release.
    Closes: bug#817282.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 09 Mar 2016 20:07:42 +0100
